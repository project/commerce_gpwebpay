<?php

namespace Drupal\commerce_gpwebpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\InvalidResponseException as CommerceInvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_gpwebpay",
 *   label = "GP Webpay",
 *   display_label = @Translation("Credit Card"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_gpwebpay\PluginForm\PaymentGPWebpayForm",
 *   },
 *   credit_card_types = {
 *     "mastercard", "maestro", "visa", "dinersclub"
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class GPWebpay extends OffsitePaymentGatewayBase {

  /**
   * Logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.channel.commerce_gpwebpay')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'private_key_path' => '',
      'passphrase' => '',
      'certificate_path' => '',
      'pay_method' => '',
      'addinfo' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant id'),
      '#description' => $this->t('E-shop id in GP'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['private_key_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key path'),
      '#description' => $this->t('This is a path to private key for GP Webpay.'),
      '#default_value' => $this->configuration['private_key_path'],
      '#required' => TRUE,
    ];

    $form['passphrase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key passphrase'),
      '#description' => $this->t('This is a passphrase that is required for the private key'),
      '#default_value' => $this->configuration['passphrase'],
      '#required' => TRUE,
    ];

    $form['certificate_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Certificate path'),
      '#description' => $this->t('A path to the public ceritifcate of GP Webpay'),
      '#default_value' => $this->configuration['certificate_path'],
      '#required' => TRUE,
    ];

    $form['pay_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Default payment method'),
      '#description' => $this->t('Default payment method in GP Webpay'),
      '#options' => [
        '' => '',
        'CRD' => 'CDR',
        'APM-BTR' => 'APM-BTR',
      ],
      '#default_value' => $this->configuration['pay_method'],
    ];

    $form['addinfo'] = [
      '#type' => 'textarea',
      '#title' => $this->t('ADDINFO'),
      '#description' => $this->t('Mandatory values: Cardholder name, Email address or Telephone number. Tokens can be used if available.'),
      '#default_value' => $this->configuration['addinfo'],
      '#required' => TRUE,
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['tokens'] = [
        '#title' => $this->t('Tokens'),
        '#type' => 'container',
      ];
      $form['tokens']['help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['commerce_order'],
        '#global_types' => FALSE,
        '#dialog' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // @todo Add validation of paths.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['private_key_path'] = $values['private_key_path'];
      $this->configuration['passphrase'] = $values['passphrase'];
      $this->configuration['certificate_path'] = $values['certificate_path'];
      $this->configuration['pay_method'] = $values['pay_method'];
      $this->configuration['addinfo'] = $values['addinfo'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    $response = $request->query->all();

//\Drupal::logger('commerce_gpwebpay response')->notice(print_r($response, true));

    $data = $response;
    
    $digest = base64_decode($data['DIGEST']);
    unset($data['DIGEST']);

    $digest1 = base64_decode($data['DIGEST1']);
    unset($data['DIGEST1']);

    $data = implode('|', $data);

    $gpwebpay_api = \Drupal::service('commerce_gpwebpay.gpwebpay_api');
    $gpwebpay_api->setConfiguration($this->configuration);

    $verified = $gpwebpay_api->verify($data, $digest) && $gpwebpay_api->verify($data.'|'.$this->configuration['merchant_id'], $digest1);
    if (!$verified) {
      throw new CommerceInvalidResponseException("Failed to verify GP response");
    }

    if ($response['MERORDERNUM'] != $order->id()) {
      throw new CommerceInvalidResponseException(sprintf("Order number for order %d does not match %d", $order->id(), $response['MERORDERNUM']));
    }

    if (($response['PRCODE'] != 0) or ($response['SRCODE'] != 0)) {
      throw new PaymentGatewayException(sprintf("Transaction failure. PRCODE: %d SRCODE: %d", $response['PRCODE'], $response['SRCODE']));
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $response['ORDERNUMBER'],
      'remote_state' => $response['PRCODE'],
    ]);
    $payment->save();
  }

}