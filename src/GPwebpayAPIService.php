<?php
namespace Drupal\commerce_gpwebpay;

class GPwebpayAPIService {

  public $test; 
  public $private_key_file, $private_key_password, $public_key_file;
  public $merchant_number;
  public $return_url;
  public $pay_method;
  public $addinfo;

  public function __construct() {
    $this->test = true;
    $this->pay_method = null;
    $this->addinfo = null;
  }

  public function setConfiguration(array $configuration) {
    $this->test = $configuration['mode'] != 'live';
    $this->private_key_file = $configuration['private_key_path'];
    $this->private_key_password = $configuration['passphrase'];
    $this->public_key_file = $configuration['certificate_path'];
    $this->merchant_number = $configuration['merchant_id'];
    $this->pay_method = $configuration['pay_method'];
    $this->addinfo = $configuration['addinfo'];
  }
  
  public function sign($data) {
    $private_key = openssl_get_privatekey(file_get_contents($this->private_key_file), $this->private_key_password);
	openssl_sign($data, $signature, $private_key);
    return $signature;
  }  

  public function verify($data, $signature) {
	$public_key = openssl_get_publickey(file_get_contents($this->public_key_file));
	return openssl_verify($data, $signature, $public_key);
  }

  public function getCreateOrderUrl($order) {
	$description = '';
	$md = '';
/*
    $recurrent = false;
    $payMethods = null;
    $referenceNumber = null;
    $lang = null;
*/
    $currencies = [
      'CZK' => 203,
      'EUR' => 978,
      'GBP' => 826,
      'USD' => 840,
    ];

	$data = [];
	$data['MERCHANTNUMBER'] = $this->merchant_number;
	$data['OPERATION'] = 'CREATE_ORDER';
	$data['ORDERNUMBER'] = $this->getRemoteId($order->id());
	$data['AMOUNT'] = $order->getTotalPrice()->getNumber() * 100;
	$data['CURRENCY'] = $currencies[$order->getTotalPrice()->getCurrencyCode()];
	$data['DEPOSITFLAG'] = 1;
	$data['MERORDERNUM'] = $order->id();
	$data['URL'] = $this->return_url;
	$data['DESCRIPTION'] = trim($description);
	$data['MD'] = trim($md);

/*
	if ($recurrent) {
      $data['USERPARAM1'] = 'R';
	}
*/
	if (!empty($this->pay_method)) {
      $data['PAYMETHOD'] = $this->pay_method;
	}
/*
	if ($payMethods !== null) {
      $data['PAYMETHODS'] = $payMethods;
	}
*/
    $email = $order->getEmail();
	if (!empty($email)) {
      $data['EMAIL'] = $email;
    }  
/*
	if ($referenceNumber !== null) {
      $data['REFERENCENUMBER'] = $referenceNumber;
	}
*/
	if (!empty($this->addinfo)) {
      $addinfo = $this->addinfo;
      if (\Drupal::moduleHandler()->moduleExists('token')) {
        $addinfo = \Drupal::token()->replace($addinfo, ['commerce_order' => $order], ['clear' => TRUE]);      
      }  
      $data['ADDINFO'] = trim(str_replace("\t", " ", str_replace("\r", "", str_replace("\n", " ", $addinfo))));
	}

	$data['DIGEST'] = base64_encode($this->sign(implode('|', $data)));
/*
	if ($lang !== null) {
      $data['LANG'] = $lang;
	}
*/
    if ($this->test) {
      $http_url = 'https://test.3dsecure.gpwebpay.com/pgw/order.do';
    }
    else {
      $http_url = 'https://3dsecure.gpwebpay.com/pgw/order.do';
    }     

    return $http_url.'?'.http_build_query($data, '', '&');
  }

  /**
   * Calculate Remote ID.
   *
   * We are concatenating order ID and timestamp of the payment. We will first
   * cut down order ID down to 5 digits and in the end we will cut down whole
   * string down to 15 digits.
   *
   * @param $orderId
   *   The ID of the order.
   *
   * @return int
   *   Generated Remote ID. This ID can be used for tracking down the payment.
   */
  private function getRemoteId($orderId): int {
    // Cut Order ID down to 5 characters.
    $shortenOrderId = str_pad(substr($orderId, -5), 5, 0, STR_PAD_LEFT);
    $timestamp = time();
    $remoteId = $timestamp . $shortenOrderId;
    // Cut down concatenated Remote ID to 15 chars.
    $remoteId = substr($remoteId, -15);
    return (int) $remoteId;
  }

}