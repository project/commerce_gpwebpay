<?php

namespace Drupal\commerce_gpwebpay\PluginForm;

use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * PaymentGPWebpayForm class.
 */
class PaymentGPWebpayForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $payment_gateway = $payment->getPaymentGateway();

    $order = $payment->getOrder();

    $gpwebpay_api = \Drupal::service('commerce_gpwebpay.gpwebpay_api');
    $gpwebpay_api->setConfiguration($payment_gateway->getPluginConfiguration());
    $gpwebpay_api->return_url = $form['#return_url'];
    $redirect_url = $gpwebpay_api->getCreateOrderUrl($order);  

    return $this->buildRedirectForm($form, $form_state, $redirect_url, [], self::REDIRECT_GET);
  }

}